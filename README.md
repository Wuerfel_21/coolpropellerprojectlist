# Cool Propeller Project List
![logo](assets/logo_full.jpg "Cool P1 Project List")

*The one and only...*

~~[(Thanks for the logo, retro wave text generator thing..)](https://m.photofunia.com/categories/all_effects/retro-wave)~~

**A list of Propeller projects I worked on or find cool. Mostly Video/Audio/Gaming/Homebrew-Computing related.**

Most of this is Propeller 1, but some P2 stuff is here, too. This is noted.

**[I put my own projects first, but fear not, just click here to jump to the other section](#others-projects)**

# My Projects
## Fully released
These are more-or-less done and/or stable

### Spin Hexagon (P1 and P2)
![shlogo](assets/spinhexagon.png "SPIN HEXAGON")

Like, Super Hexagon but written in Spin :)
 - [GitHub (P1)](https://github.com/IRQsome/Spin-Hexagon)
 - [Parallax Forum thread (P1)](https://forums.parallax.com/discussion/171980/new-p1-game-spin-hexagon/p1)
 - [Parallax Forum thread (P2)](Spin Hexagon)


### Turbulence NTSC (P1)
LFT's _Turblulence_ demo modified for NTSC composite output
 - [Parallax Forum thread](https://forums.parallax.com/discussion/170304/ntsc-version-of-lfts-turbulence-demo)
 - [Youtube (60FPS!)](https://www.youtube.com/watch?v=JG8ai-OkMeQ)

### JET Engine (P1)
Tile/Sprite video driver with a focus on features, compact graphics data and high sprite count
- [Parallax Forum thread](https://forums.parallax.com/discussion/169060/release-jet-engine-new-tile-sprite-graphics-driver)
- Can also be found in OBEX

### TinySDDA (P1)
Plays 16 bit stereo music and A-Law sound effects from an SD card in just one cog.
- [Parallax Forum thread](https://forums.parallax.com/discussion/171949/tinysdda-v1-0-sdcard-sector-r-w-stereo-music-sound-effects-in-one-cog-and-almost-no-hub-ram)

### Retronitus DX (P1, P2 version in preview state)
![DX Logo](https://gitlab.com/irqsome/retronitus-dx/-/raw/master/logo/rsndlogo_8by7.png "Noise from the past!")
Continued development of Johannes Ahlebrand's _Retronitus_ synthesizer object.
 - [Parallax Forum thread](https://forums.parallax.com/discussion/132826/retronitus-noise-from-the-past)
 - [GitLab](https://gitlab.com/irqsome/retronitus-dx)

### OPN2cog (P2)
![OPN2cog](assets/opn2_logo_verybig.png "OPN2cog logo")
Yamaha YM2612 emulator for P2.
  - [Parallax Forum thread](https://forums.parallax.com/discussion/173385/opn2cog-the-sound-of-the-sega-mega-drive-in-a-cog)
  - Can also be found in OBEX

### VentilatorOS (P1)
Enhanced fork of Cluso's P1 OS. 
 - [GitLab](https://gitlab.com/irqsome/ventilator-os)
 - [Relevant forum comment](https://forums.parallax.com/discussion/comment/1469836/#Comment_1469836)

### Ventilator Software archive (P1)
Archive of software (mostly games and other homecomputer-y applications) optimized for VentilatorOS and the Ventilator computer.
Also includes videos of nearly all of them in action.
 - [GitLab](https://gitlab.com/irqsome/ventilator-software)
 - [Video playlist](https://www.youtube.com/playlist?list=PLjK0tF9KYgXE51mRW94vNo1vqm25h-MVL)

### PixCRUSH (P1)
![pcr](assets/reimu_banner.jpg "CRT closeup of reimu.dat being displayed")

A picture display application. It works around RAM being to small to hold a full 256x224 frame by decompressing the image in real time as it is scanned out.
 - [Parallax Forum thread](https://forums.parallax.com/discussion/comment/1515941/#Comment_1515941)


### SNES2COM (P1)
Code and instructions for forwarding a SNES controller to a PC
 - [Parallax Forum thread](https://forums.parallax.com/discussion/170586/snes2com-use-a-gamepad-connected-to-your-propeller-with-pc-games)
 - [In the archive](https://gitlab.com/irqsome/ventilator-software/-/tree/master/utils/snes2com)

### Random forum threads
These are some threads containing whatever random wisdom or small bits of code. May be outdated or wrong.
 - [P8XNICCC: ST-NICCC 2000 demo on P1!](https://forums.parallax.com/discussion/172607/p8xniccc-st-niccc-2000-demo-on-p1)
 - [VocalTract sings UTAU UST files (Jape Project)](https://forums.parallax.com/discussion/172238/so-i-tried-to-make-vocaltract-spin-sing-ust-files)
 - [SNES Mouse driver](https://forums.parallax.com/discussion/172312/release-snes-mouse-driver)
 - [A ROM font compatible 8x8 font](https://forums.parallax.com/discussion/167894/a-rom-font-compatibile-8x8-font)
 - [Spin1/Ruby polyglot](https://forums.parallax.com/discussion/169925/spin1-ruby-polyglot)
 - [PAL60 mode on the propeller](https://forums.parallax.com/discussion/168916/pal60-mode-on-the-propeller)
 - [Further observations on PAL(60) and NTSC](https://forums.parallax.com/discussion/169057/further-observations-on-pal-60-and-ntsc)
 - [S-Video gush thread](https://forums.parallax.com/discussion/172069/s-video-gush-thread)
 - [8bitdo Retro Reciever works with the Propeller](https://forums.parallax.com/discussion/172219/8bitdo-retro-reciever-bluetooth-gamepad-dongle)
 - [20€ Ebay tomato monitor for NTSC/PAL - it's actually good?!?](https://forums.parallax.com/discussion/172761/20-ebay-tomato-monitor-for-ntsc-pal-its-actually-good)
 - [The actually functional Spin2 SD Driver (I hope) (KyeFAT+ sdspi_with_audio)](https://forums.parallax.com/discussion/173378/the-actually-functional-spin2-sd-driver-i-hope-kyefat-sdspi-with-audio)
 - [P2 console emulation spitballing](https://forums.parallax.com/discussion/173381/console-emulation)

## Work-in-Progress
These are not done yet, but have some material available to download and mess around with.

### Vector JET (P1 and P2)
(abbrv. VJET)
Graphics driver for rendering double-buffered, full color, 256x238 filled polygon graphics without using a lot of RAM. (A version of this is used in [_Spin Hexagon_](#spin-hexagon))
 - [Parallax Forum thread (P1)](https://forums.parallax.com/discussion/171856/wip-beam-chasing-polygon-rasterizer)
 - [Parallax Forum thread (P2)](https://forums.parallax.com/discussion/172982/super-duper-early-preview-vjet-for-p2)

### Ventilator Computer (P1)
(as-in the german word for "fan" (haha get it?), not the medical device...)

My trusty Propeller 1 board. Still have to do a proper website for it. Also need to do a Rev. B to fix the HW bugs. Also have to convert the project to another CAD program, since EAGLE has since switched to a subscription model, which I do not want to endorse in any way.

[Anyways, here's the schematic in case you want to build a compatible board](assets/Ventilator Computer.sch.pdf) (**Please note that this is not 100% perfect.** The two currently known schematic errors: VGA shield is not grounded and the auto-reset circuit is kinda broken ~~(put a 2.2kΩ either side of the auto-reset enable jumper to make it work semi-reliably) )~~ (put a 3.3k in-line with the RTS line and the outgoing data line)


### Spinner 2 (P2)
C-based frontend to the official Spin2 compiler.

 - [Forum thread](https://forums.parallax.com/discussion/174141/spinner2-command-line-spin2-compiler-for-windows-and-linux)


## Upcoming
These are not done at all

### Projekt Menetekel

Somewhat Zelda-like game with a storyline, big world to explore and enemies to shoot at (or rather, get shot by...)

Release timeframe: unknown.

 - [Forum thread](https://forums.parallax.com/discussion/173051/wip-projekt-menetekel-that-game-i-keep-talking-about/p1)

Estimated P1 H/W requirements:
 - P1 with CLKFREQ => 80 MHz 
 - SD card
 - (not required but recommended) 64K SPI RAM
 - standard TV or VGA output (RGB332 VGA is also supported)
 - Input device with 8-way directional input and at least 4 buttons (6 buttons strongly recommended, SNES-like layout recommended)


# Other's Projects
**(This section is still incomplete, I will add more later...)**

## Propeller Discord
Discord server for P2 and P1 real-time chat by QuelloRobotics
 - [CLICK HERE TO JOIN](https://discord.gg/9TkW8jD)

## Flexspin / spin2cpp
Multi-language compiler suite for P1 and P2.
I've actually contributed quite a bit to this one...
 - [GitHub](https://github.com/totalspectrum/spin2cpp)

## Homespun
Spin compiler in C#. Notable for having a working preprocessor, unlike OpenSpin.
I've kinda taken over maintaining it, but I don't want to claim it as my work, so it's in this section.
 - [Latest bugfix build (No. 2)](https://forums.parallax.com/discussion/download/129197/homespun-0.32p2.zip) (thanks, @yeti)
 - [Parallax Forum thread](https://forums.parallax.com/discussion/148260/homespun-compiler-open-source/p2)

## Rayman's site
Lots of useful info, but slightly disorganized. Check the "Programming Examples" section, there's some useful PC tools in there
 - [Link](http://www.rayslogic.com/propeller/propeller.htm)

## JT Cook's site
JT wrote a lot of good Propeller games, which can be found on his website (most of them are also in the [Software Archive](#ventilator-software-archive))
 - [Link](http://avalondreams.com/menu.html) 
 - [C3 Section that is not actually linked anywhere](http://avalondreams.com/c3/index.html)

## WATCH THIS SPACE
Is your project
 - Cool, Radical or EXTREME?
 - Underappreciated and in need of attention?
 - Related to or applicable to my sphere of interest (Video/Audio/Gaming/Homebrew-Computing)?

Then (or if you found anything else that's wrong on this page) send a Merge Request to this repository or hit me up on the forums or Discord to add it here.


